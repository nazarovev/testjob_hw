<footer class="footer">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12">
                © <script>document.write(new Date().getFullYear())</script> Nazarov Evgeniy <span class="d-none d-sm-inline-block"> - for Hobby World.</span>
            </div>
        </div>
    </div>
</footer>
