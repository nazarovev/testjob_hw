<!doctype html>
<html lang="ru">

@include('layouts.head')

<body data-topbar="dark" data-layout="horizontal" data-layout-size="boxed">

<!-- Begin page -->
<div id="layout-wrapper">

    @include('layouts.header')

    @include('layouts.topnav')

    <!-- ============================================================== -->
    <!-- Start right Content here -->
    <!-- ============================================================== -->
    <div class="main-content">

        <div class="page-content">
            @yield('content')
        </div>
        <!-- End Page-content -->



        @include('layouts.footer')

    </div>
    <!-- end main content-->

</div>
<!-- END layout-wrapper -->

<!-- JAVASCRIPT -->
@include('layouts.scripts')

</body>
</html>
