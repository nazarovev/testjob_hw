@extends('layouts.app')

@section('content')
    <div class="container-fluid">


        <div class="row mt-4">
            <div class="col-12">
                <div class="card">
                    <div class="card-body">


                        <div class="row">



                            <div class="col-12">
                                <div class="text-center m-4">
                                    <button type="button" class="btn btn-primary waves-effect waves-light">Загрузить статьи</button>
                                </div>

                            </div>


                        </div> <!-- end row -->
                    </div>
                </div>
            </div>
        </div><!-- end row -->

        <div class="row">
            <div class="col-12">
                <div class="card">
                    <div class="card-header">
                        <h4 class="card-title">Название статьи</h4>
                        <p>Теги статьи</p>
                    </div>
                    <div class="card-body">


                        <div class="row">



                            <div class="col-12">
                                <div class="post-image text-center mb-4">
                                    <img src="{{asset('assets/images/gallery/work-1.jpg')}}" alt="" style="max-height: 400px;">
                                </div>
                                <div class="post-content">
                                    Краткое содержание статьи до 200 символов
                                </div>

                                <div class="text-center">
                                    <button type="button" class="btn btn-primary waves-effect waves-light" data-toggle="modal" data-target=".bs-example-modal-lg">Полный текст</button>
                                </div>

                            </div>


                        </div> <!-- end row -->
                    </div>
                </div>
            </div>
        </div><!-- end row -->



    </div> <!-- container-fluid -->


    <!--  Modal content for the above example -->
    <div class="modal fade bs-example-modal-lg" tabindex="-1" role="dialog" aria-labelledby="myLargeModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-lg" style="max-width: 96%;">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title mt-0" id="myLargeModalLabel">Название статьи</h5>
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">×</button>
                </div>
                <div class="modal-body">
                    <p class="font-weight-bold">Теги статьи</p>
                    <p>СОДЕРЖАНИЕ СТАТЬИ</p>
                </div>
            </div><!-- /.modal-content -->
        </div><!-- /.modal-dialog -->
    </div><!-- /.modal -->

@endsection
